class Logger {
  private static instance: Logger;

  private constructor() {}

  private appendLog(poruka: string, brojStola: string, naplata: number) {
    const moment = new Date(Date.now());
    const logLine = `${poruka}: ${moment.toDateString()} ${moment.toLocaleTimeString()} ${naplata.toFixed(
      2
    ) || ''}\n`;
    console.log(logLine);
  }

  public static getInstance() {
    return this.instance || (this.instance = new this());
  }

  public log(poruka: string, brojStola: number, naplata: number = 0) {
    this.appendLog(poruka, brojStola.toString(), naplata);
  }
}

const logger = Logger.getInstance();

abstract class Hrana {
  naziv: string;
  abstract cena: number;
  constructor(naizv: string) {
    this.naziv = naizv;
  }
}

class Pizza extends Hrana {
  cena: number;
  tip: string = 'Pizza';

  constructor(naziv: string) {
    super(naziv);
    this.cena = Math.random() * (600 - 200) + 200;
  }

  getNaziv() {
    return `${this.tip} ${this.naziv}`;
  }
}

class Pasta extends Pizza {
  tip: string = 'Pasta';
  constructor(naziv: string) {
    super(naziv);
  }
}

class Prilog extends Hrana {
  cena: number;

  constructor(naziv: string) {
    super(naziv);
    this.cena = Math.random() * (100 - 20) + 20;
  }

  getNaziv() {
    return `+ ${this.naziv}`;
  }
}

abstract class Pice extends Hrana {
  zapremina: number;
  cena: number;
  constructor(naziv: string, zapremina: number) {
    super(naziv);
    this.zapremina = zapremina;
    this.cena = Math.random() * (500 - 150) + 150;
  }
}

class Voda extends Pice {
  tip: string = 'Voda';
  constructor(naziv: string, zapremina: number) {
    super(naziv, zapremina);
  }
}

class GaziraniSok extends Pice {
  tip: string = 'Gazirani sok';
  constructor(naziv: string, zapremina: number) {
    super(naziv, zapremina);
  }
}

class NegaziraniSok extends Pice {
  tip: string = 'Negazirani sok';
  constructor(naziv: string, zapremina: number) {
    super(naziv, zapremina);
  }
}

class MeniFactory {
  constructor() {}
  kreirajHranu(tip: string, naizv: string) {
    if (tip.toLowerCase() === 'pizza') return new Pizza(naizv);
    if (tip.toLowerCase() === 'passta') return new Pasta(naizv);
    if (tip.toLowerCase() === 'prilog') return new Prilog(naizv);
  }

  kreirajPice(tip: string, naziv: string, zapremina: number) {
    if (!naziv) naziv = tip;
    if (!zapremina) zapremina = 1;

    if (tip.toLowerCase() === 'gazirani sok')
      return new GaziraniSok(naziv, zapremina);
    else if (tip.toLowerCase() === 'negazirani sok')
      return new NegaziraniSok(naziv, zapremina);
    else return new Voda(naziv, zapremina);
  }
}

class StavkaPorudzbine {
  artikal: Hrana;
  kolicina: number;

  constructor(artikal: any, kolicina = 1) {
    this.artikal = artikal;
    this.kolicina = kolicina;
  }
}

class Sto {
  brojStola: number;
  porudzbina: StavkaPorudzbine[];

  constructor(brojStola: number) {
    this.brojStola = brojStola;
    this.porudzbina = [];
  }

  primiPorudzbinu(porudzbina: StavkaPorudzbine[]) {
    if (this.porudzbina.length > 0) {
      throw new Error(
        'Nije moguće izdati novu porudžbinu jer prethodna nije plaćena'
      );
    } else {
      this.porudzbina = porudzbina;
      logger.log('Porudžbina', this.brojStola);
    }
  }

  platiPorudzbinu() {
    if (this.porudzbina.length < 1)
      throw new Error('Nema neplacenih porudzbina!');
    let suma = 0;

    this.porudzbina.forEach(stavka => {
      suma += stavka.artikal.cena * stavka.kolicina;
    });
    this.porudzbina = [];
    logger.log('Račun', this.brojStola, suma);
  }
}

const meni = new MeniFactory();

const pizzaCapricoza = meni.kreirajHranu('pizza', 'Capricciosa');
const pizzaItaliana = meni.kreirajHranu('pizza', 'Italiana');
const pizzaSiciliana = meni.kreirajHranu('pizza', 'Siciliana');
const pizzaMadjarica = meni.kreirajHranu('pizza', 'Madjarica');
const pastaCarbonara = meni.kreirajHranu('passta', 'Carbonara');
const pastaMilano = meni.kreirajHranu('passta', 'Milano');
const pastaTorino = meni.kreirajHranu('passta', 'Torino');
const pastaVerona = meni.kreirajHranu('passta', 'Verona');
const pastaRome = meni.kreirajHranu('passta', 'Rome');
const piceCola = meni.kreirajPice('gazirani sok', 'Coca Cola', 0.5);
const piceNegaziraniSok = meni.kreirajPice('negazirani sok', '', 0);
const prilogKecap = meni.kreirajHranu('prilog', 'Kecap');
const prilogOrigano = meni.kreirajHranu('prilog', 'Origano');
const prilogBosiljak = meni.kreirajHranu('prilog', 'Bosiljak');
const prilogPelat = meni.kreirajHranu('prilog', 'Pelat');
const prilogMajonez = meni.kreirajHranu('prilog', 'Majonez');

// console.log(pizzaCapricoza);

const sto1 = new Sto(1);
const sto2 = new Sto(2);
const sto3 = new Sto(3);

sto1.primiPorudzbinu([
  new StavkaPorudzbine(pizzaCapricoza, 1),
  new StavkaPorudzbine(pastaCarbonara, 2),
  new StavkaPorudzbine(prilogKecap, 2),
  new StavkaPorudzbine(piceNegaziraniSok, 1)
]);

sto2.primiPorudzbinu([
  new StavkaPorudzbine(pizzaCapricoza, 1),
  new StavkaPorudzbine(pastaCarbonara, 2),
  new StavkaPorudzbine(prilogKecap, 2),
  new StavkaPorudzbine(piceNegaziraniSok, 1)
]);

sto3.primiPorudzbinu([
  new StavkaPorudzbine(pizzaCapricoza, 1),
  new StavkaPorudzbine(pastaCarbonara, 2),
  new StavkaPorudzbine(prilogKecap, 2),
  new StavkaPorudzbine(piceCola, 4)
]);

sto1.platiPorudzbinu();
sto3.platiPorudzbinu();

// sto2.primiPorudzbinu([
//   new StavkaPorudzbine(pizzaCapricoza, 1),
//   new StavkaPorudzbine(pastaCarbonara, 2),
//   new StavkaPorudzbine(prilogKecap, 2),
//   new StavkaPorudzbine(piceNegaziraniSok, 1)
// ]);

sto2.platiPorudzbinu();
