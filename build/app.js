"use strict";
class Logger {
    constructor() { }
    appendLog(poruka, brojStola, naplata) {
        const moment = new Date(Date.now());
        const logLine = `${poruka}: ${moment.toDateString()} ${moment.toLocaleTimeString()} ${naplata.toFixed(2) || ''}\n`;
        console.log(logLine);
    }
    static getInstance() {
        return this.instance || (this.instance = new this());
    }
    log(poruka, brojStola, naplata = 0) {
        this.appendLog(poruka, brojStola.toString(), naplata);
    }
}
const logger = Logger.getInstance();
class Hrana {
    constructor(naizv) {
        this.naziv = naizv;
    }
}
class Pizza extends Hrana {
    constructor(naziv) {
        super(naziv);
        this.tip = 'Pizza';
        this.cena = Math.random() * (600 - 200) + 200;
    }
    getNaziv() {
        return `${this.tip} ${this.naziv}`;
    }
}
class Pasta extends Pizza {
    constructor(naziv) {
        super(naziv);
        this.tip = 'Pasta';
    }
}
class Prilog extends Hrana {
    constructor(naziv) {
        super(naziv);
        this.cena = Math.random() * (100 - 20) + 20;
    }
    getNaziv() {
        return `+ ${this.naziv}`;
    }
}
class Pice extends Hrana {
    constructor(naziv, zapremina) {
        super(naziv);
        this.zapremina = zapremina;
        this.cena = Math.random() * (500 - 150) + 150;
    }
}
class Voda extends Pice {
    constructor(naziv, zapremina) {
        super(naziv, zapremina);
        this.tip = 'Voda';
    }
}
class GaziraniSok extends Pice {
    constructor(naziv, zapremina) {
        super(naziv, zapremina);
        this.tip = 'Gazirani sok';
    }
}
class NegaziraniSok extends Pice {
    constructor(naziv, zapremina) {
        super(naziv, zapremina);
        this.tip = 'Negazirani sok';
    }
}
class MeniFactory {
    constructor() { }
    kreirajHranu(tip, naizv) {
        if (tip.toLowerCase() === 'pizza')
            return new Pizza(naizv);
        if (tip.toLowerCase() === 'passta')
            return new Pasta(naizv);
        if (tip.toLowerCase() === 'prilog')
            return new Prilog(naizv);
    }
    kreirajPice(tip, naziv, zapremina) {
        if (!naziv)
            naziv = tip;
        if (!zapremina)
            zapremina = 1;
        if (tip.toLowerCase() === 'gazirani sok')
            return new GaziraniSok(naziv, zapremina);
        else if (tip.toLowerCase() === 'negazirani sok')
            return new NegaziraniSok(naziv, zapremina);
        else
            return new Voda(naziv, zapremina);
    }
}
class StavkaPorudzbine {
    constructor(artikal, kolicina = 1) {
        this.artikal = artikal;
        this.kolicina = kolicina;
    }
}
class Sto {
    constructor(brojStola) {
        this.brojStola = brojStola;
        this.porudzbina = [];
    }
    primiPorudzbinu(porudzbina) {
        if (this.porudzbina.length > 0) {
            throw new Error('Nije moguće izdati novu porudžbinu jer prethodna nije plaćena');
        }
        else {
            this.porudzbina = porudzbina;
            logger.log('Porudžbina', this.brojStola);
        }
    }
    platiPorudzbinu() {
        if (this.porudzbina.length < 1)
            throw new Error('Nema neplacenih porudzbina!');
        let suma = 0;
        this.porudzbina.forEach(stavka => {
            suma += stavka.artikal.cena * stavka.kolicina;
        });
        this.porudzbina = [];
        logger.log('Račun', this.brojStola, suma);
    }
}
const meni = new MeniFactory();
const pizzaCapricoza = meni.kreirajHranu('pizza', 'Capricciosa');
const pizzaItaliana = meni.kreirajHranu('pizza', 'Italiana');
const pizzaSiciliana = meni.kreirajHranu('pizza', 'Siciliana');
const pizzaMadjarica = meni.kreirajHranu('pizza', 'Madjarica');
const pastaCarbonara = meni.kreirajHranu('passta', 'Carbonara');
const pastaMilano = meni.kreirajHranu('passta', 'Milano');
const pastaTorino = meni.kreirajHranu('passta', 'Torino');
const pastaVerona = meni.kreirajHranu('passta', 'Verona');
const pastaRome = meni.kreirajHranu('passta', 'Rome');
const piceCola = meni.kreirajPice('gazirani sok', 'Coca Cola', 0.5);
const piceNegaziraniSok = meni.kreirajPice('negazirani sok', '', 0);
const prilogKecap = meni.kreirajHranu('prilog', 'Kecap');
const prilogOrigano = meni.kreirajHranu('prilog', 'Origano');
const prilogBosiljak = meni.kreirajHranu('prilog', 'Bosiljak');
const prilogPelat = meni.kreirajHranu('prilog', 'Pelat');
const prilogMajonez = meni.kreirajHranu('prilog', 'Majonez');
// console.log(pizzaCapricoza);
const sto1 = new Sto(1);
const sto2 = new Sto(2);
const sto3 = new Sto(3);
sto1.primiPorudzbinu([
    new StavkaPorudzbine(pizzaCapricoza, 1),
    new StavkaPorudzbine(pastaCarbonara, 2),
    new StavkaPorudzbine(prilogKecap, 2),
    new StavkaPorudzbine(piceNegaziraniSok, 1)
]);
sto2.primiPorudzbinu([
    new StavkaPorudzbine(pizzaCapricoza, 1),
    new StavkaPorudzbine(pastaCarbonara, 2),
    new StavkaPorudzbine(prilogKecap, 2),
    new StavkaPorudzbine(piceNegaziraniSok, 1)
]);
sto3.primiPorudzbinu([
    new StavkaPorudzbine(pizzaCapricoza, 1),
    new StavkaPorudzbine(pastaCarbonara, 2),
    new StavkaPorudzbine(prilogKecap, 2),
    new StavkaPorudzbine(piceCola, 4)
]);
sto1.platiPorudzbinu();
sto3.platiPorudzbinu();
// sto2.primiPorudzbinu([
//   new StavkaPorudzbine(pizzaCapricoza, 1),
//   new StavkaPorudzbine(pastaCarbonara, 2),
//   new StavkaPorudzbine(prilogKecap, 2),
//   new StavkaPorudzbine(piceNegaziraniSok, 1)
// ]);
sto2.platiPorudzbinu();
